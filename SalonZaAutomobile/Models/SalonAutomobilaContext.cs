﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SalonZaAutomobile.Models
{
    public class SalonAutomobilaContext : DbContext
    {
        public DbSet<SalonAutomobila> SalonAutomobilas { get; set; }
        public DbSet<Automobil> Automobils { get; set; }
        public DbSet<Proizvodjac> Proizvodjacs { get; set; }
        public DbSet<Ugovor> Ugovors { get; set; }

        public SalonAutomobilaContext() : base("name=SalonAutomobilaContext")
        {

        }
    }
}