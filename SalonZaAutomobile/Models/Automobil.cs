﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonZaAutomobile.Models
{
    public class Automobil
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Model { get; set; }

        [Range(1950, int.MaxValue)]
        public int GodinaProizvodnje { get; set; }


        [Range(700, int.MaxValue)]
        public int Kubikaza { get; set; }

        [Required]
        [StringLength(20)]
        public string Boja { get; set; }

        public int ProizvodjacId { get; set; }

        public int SalonAutomobilaId { get; set; }

        [ForeignKey("ProizvodjacId")]
        public Proizvodjac Proizvodjac { get; set; }

        [ForeignKey("SalonAutomobilaId")]
        public SalonAutomobila SalonAutomobila { get; set; }
    }
}