﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SalonZaAutomobile.Models
{
    public class SalonAutomobila
    {
        public int Id { get; set; }

        //[RegularExpression(@"\D{6}\")]
        public int Pib { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Naziv salona")]
        public string Naziv { get; set; }

        [Required]
        [StringLength(20)]
        public string Drzava { get; set; }

        [Required]
        [StringLength(20)]
        public string Grad { get; set; }

        [Required]
        [StringLength(20)]
        public string Adresa { get; set; }
        public List<Automobil> AutomobiliSalona { get; set; }
        public List<Ugovor> UgovoriSalona { get; set; }
    }
}