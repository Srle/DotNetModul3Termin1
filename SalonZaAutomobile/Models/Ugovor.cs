﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SalonZaAutomobile.Models
{
    public class Ugovor
    {
        public int Id { get; set; }

        public int SalonAutomobilaId { get; set; }

        public int ProizvodjacId { get; set; }

        [ForeignKey("SalonAutomobilaId")]
        public SalonAutomobila SalonAutomobila { get; set; }

        [ForeignKey("ProizvodjacId")]
        public Proizvodjac Proizvodjac { get; set; }
    }
}