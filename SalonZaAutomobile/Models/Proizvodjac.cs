﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SalonZaAutomobile.Models
{
    public class Proizvodjac
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Naziv { get; set; }

        [Required]
        [StringLength(50)]
        public string Drzava { get; set; }

        [Required]
        [StringLength(50)]
        public string Grad { get; set; }

        public List<Automobil> AutomobiliProizvodjaca { get; set; }
        public List<Ugovor> UgovoriProizvodjaca { get; set; }
    }
}