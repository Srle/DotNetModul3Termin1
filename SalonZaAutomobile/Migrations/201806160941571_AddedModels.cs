namespace SalonZaAutomobile.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Automobils",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false, maxLength: 50),
                        GodinaProizvodnje = c.Int(nullable: false),
                        Kubikaza = c.Int(nullable: false),
                        Boja = c.String(nullable: false, maxLength: 20),
                        ProizvodjacId = c.Int(nullable: false),
                        SalonAutomobilaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proizvodjacs", t => t.ProizvodjacId, cascadeDelete: true)
                .ForeignKey("dbo.SalonAutomobilas", t => t.SalonAutomobilaId, cascadeDelete: true)
                .Index(t => t.ProizvodjacId)
                .Index(t => t.SalonAutomobilaId);
            
            CreateTable(
                "dbo.Proizvodjacs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(nullable: false, maxLength: 50),
                        Drzava = c.String(nullable: false, maxLength: 50),
                        Grad = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ugovors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalonAutomobilaId = c.Int(nullable: false),
                        ProizvodjacId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Proizvodjacs", t => t.ProizvodjacId, cascadeDelete: true)
                .ForeignKey("dbo.SalonAutomobilas", t => t.SalonAutomobilaId, cascadeDelete: true)
                .Index(t => t.SalonAutomobilaId)
                .Index(t => t.ProizvodjacId);
            
            CreateTable(
                "dbo.SalonAutomobilas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pib = c.Int(nullable: false),
                        Naziv = c.String(nullable: false, maxLength: 50),
                        Drzava = c.String(nullable: false, maxLength: 20),
                        Grad = c.String(nullable: false, maxLength: 20),
                        Adresa = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ugovors", "SalonAutomobilaId", "dbo.SalonAutomobilas");
            DropForeignKey("dbo.Automobils", "SalonAutomobilaId", "dbo.SalonAutomobilas");
            DropForeignKey("dbo.Ugovors", "ProizvodjacId", "dbo.Proizvodjacs");
            DropForeignKey("dbo.Automobils", "ProizvodjacId", "dbo.Proizvodjacs");
            DropIndex("dbo.Ugovors", new[] { "ProizvodjacId" });
            DropIndex("dbo.Ugovors", new[] { "SalonAutomobilaId" });
            DropIndex("dbo.Automobils", new[] { "SalonAutomobilaId" });
            DropIndex("dbo.Automobils", new[] { "ProizvodjacId" });
            DropTable("dbo.SalonAutomobilas");
            DropTable("dbo.Ugovors");
            DropTable("dbo.Proizvodjacs");
            DropTable("dbo.Automobils");
        }
    }
}
