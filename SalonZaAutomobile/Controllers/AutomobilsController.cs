﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonZaAutomobile.Models;

namespace SalonZaAutomobile.Controllers
{
    public class AutomobilsController : Controller
    {
        private SalonAutomobilaContext db = new SalonAutomobilaContext();

        // GET: Automobils
        public ActionResult Index()
        {
            var automobils = db.Automobils.Include(a => a.Proizvodjac).Include(a => a.SalonAutomobila);
            return View(automobils.ToList());
        }

        // GET: Automobils/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Automobil automobil = db.Automobils.Include(a => a.Proizvodjac).Include(a => a.SalonAutomobila).SingleOrDefault(x => x.Id == id);

            //Automobil automobil = automobils.Find(id);
            if (automobil == null)
            {
                return HttpNotFound();
            }
            return View(automobil);
        }
         
        // GET: Automobils/Create
        public ActionResult Create(int? id)
        {
            
            if (id != null)
            {
                SalonAutomobila s = db.SalonAutomobilas.Find(id);
                List<SalonAutomobila> salon = new List<SalonAutomobila>();
                salon.Add(s);
                ViewBag.SalonAutomobilaId = new SelectList(salon, "Id", "Naziv");

                List<Proizvodjac> sviproizvodjaci = db.Proizvodjacs.ToList();
                List<Ugovor> sviugovori = db.Ugovors.ToList();
                List<Proizvodjac> proizvodjacisalona = new List<Proizvodjac>();
                foreach (Proizvodjac p in sviproizvodjaci)
                {
                    foreach (Ugovor u in sviugovori)
                    {
                        if (p.Id==u.ProizvodjacId && u.SalonAutomobilaId==id)
                        {
                            proizvodjacisalona.Add(p);
                        }
                    }
                }
                ViewBag.ProizvodjacId = new SelectList(proizvodjacisalona, "Id", "Naziv");
            }
            else
            {
                ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv");
                ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv");
            }
            return View();
        }

        // POST: Automobils/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Model,GodinaProizvodnje,Kubikaza,Boja,ProizvodjacId,SalonAutomobilaId")] Automobil automobil)
        {
            if (ModelState.IsValid)
            {
                db.Automobils.Add(automobil);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", automobil.ProizvodjacId);
            ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv", automobil.SalonAutomobilaId);
            return View(automobil);
        }

        // GET: Automobils/Edit/5
        public ActionResult Edit(int? id, int? sid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Automobil automobil = db.Automobils.Find(id);
            if (automobil == null)
            {
                return HttpNotFound();
            }

            if (sid != null)
            {
                SalonAutomobila s = db.SalonAutomobilas.Find(sid);
                List<SalonAutomobila> salon = new List<SalonAutomobila>();
                salon.Add(s);
                ViewBag.SalonAutomobilaId = new SelectList(salon, "Id", "Naziv");

                List<Proizvodjac> sviproizvodjaci = db.Proizvodjacs.ToList();
                List<Ugovor> sviugovori = db.Ugovors.ToList();
                List<Proizvodjac> proizvodjacisalona = new List<Proizvodjac>();
                foreach (Proizvodjac p in sviproizvodjaci)
                {
                    foreach (Ugovor u in sviugovori)
                    {
                        if (p.Id == u.ProizvodjacId && u.SalonAutomobilaId == sid)
                        {
                            proizvodjacisalona.Add(p);
                        }
                    }
                }
                ViewBag.ProizvodjacId = new SelectList(proizvodjacisalona, "Id", "Naziv");
            }
            else
            {
                ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv");
                ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv");
            }
            return View(automobil);
        }

        // POST: Automobils/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Model,GodinaProizvodnje,Kubikaza,Boja,ProizvodjacId,SalonAutomobilaId")] Automobil automobil)
        {
            if (ModelState.IsValid)
            {
                db.Entry(automobil).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", automobil.ProizvodjacId);
            ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv", automobil.SalonAutomobilaId);
            return View(automobil);
        }

        // GET: Automobils/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Automobil automobil = db.Automobils.Include(a => a.Proizvodjac).Include(a => a.SalonAutomobila).SingleOrDefault(x => x.Id == id);

            if (automobil == null)
            {
                return HttpNotFound();
            }
            return View(automobil);
        }

        // POST: Automobils/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Automobil automobil = db.Automobils.Find(id);
            db.Automobils.Remove(automobil);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
