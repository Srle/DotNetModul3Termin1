﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonZaAutomobile.Models;

namespace SalonZaAutomobile.Controllers
{
    public class SalonAutomobilasController : Controller
    {
        private SalonAutomobilaContext db = new SalonAutomobilaContext();

        // GET: SalonAutomobilas
        public ActionResult Index()
        {
            return View(db.SalonAutomobilas.ToList());
        }

        // GET: SalonAutomobilas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonAutomobila salonAutomobila = db.SalonAutomobilas.Find(id);
            if (salonAutomobila == null)
            {
                return HttpNotFound();
            }

            var automobils = db.Automobils.Include(a => a.Proizvodjac).Include(a => a.SalonAutomobila).ToList();
            var ugovors = db.Ugovors.Include(u => u.Proizvodjac).Include(u => u.SalonAutomobila).ToList();


            return View(salonAutomobila);
        }

        // GET: SalonAutomobilas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SalonAutomobilas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Pib,Naziv,Drzava,Grad,Adresa")] SalonAutomobila salonAutomobila)
        {
            if (ModelState.IsValid)
            {
                db.SalonAutomobilas.Add(salonAutomobila);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(salonAutomobila);
        }

        // GET: SalonAutomobilas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonAutomobila salonAutomobila = db.SalonAutomobilas.Find(id);
            if (salonAutomobila == null)
            {
                return HttpNotFound();
            }
            return View(salonAutomobila);
        }

        // POST: SalonAutomobilas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Pib,Naziv,Drzava,Grad,Adresa")] SalonAutomobila salonAutomobila)
        {
            if (ModelState.IsValid)
            {
                db.Entry(salonAutomobila).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(salonAutomobila);
        }

        // GET: SalonAutomobilas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonAutomobila salonAutomobila = db.SalonAutomobilas.Find(id);
            if (salonAutomobila == null)
            {
                return HttpNotFound();
            }
            return View(salonAutomobila);
        }

        // POST: SalonAutomobilas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalonAutomobila salonAutomobila = db.SalonAutomobilas.Find(id);
            db.SalonAutomobilas.Remove(salonAutomobila);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
