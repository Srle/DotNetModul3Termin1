﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonZaAutomobile.Models;

namespace SalonZaAutomobile.Controllers
{
    public class UgovorsController : Controller
    {
        private SalonAutomobilaContext db = new SalonAutomobilaContext();

        // GET: Ugovors
        public ActionResult Index()
        {
            var ugovors = db.Ugovors.Include(u => u.Proizvodjac).Include(u => u.SalonAutomobila);
            return View(ugovors.ToList());
        }

        // GET: Ugovors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Include(u => u.Proizvodjac).Include(u => u.SalonAutomobila).SingleOrDefault(x => x.Id == id);
            if (ugovor == null)
            {
                return HttpNotFound();
            }
            return View(ugovor);
        }

        // GET: Ugovors/Create
        public ActionResult Create(int? id)
        {
            if (id != null)
            {
                SalonAutomobila s = db.SalonAutomobilas.Find(id);
                List<SalonAutomobila> salon = new List<SalonAutomobila>();
                salon.Add(s);
                ViewBag.SalonAutomobilaId = new SelectList(salon, "Id", "Naziv");  
            }
            else
            {
                ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv");
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv");

            return View();
        }

        // POST: Ugovors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SalonAutomobilaId,ProizvodjacId")] Ugovor ugovor)
        {
            if (ModelState.IsValid)
            {
                db.Ugovors.Add(ugovor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
            ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv", ugovor.SalonAutomobilaId);
            return View(ugovor);
        }

        // GET: Ugovors/Edit/5
        public ActionResult Edit(int? id, int? sid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Find(id);
            if (ugovor == null)
            {
                return HttpNotFound();
            }
            if (sid != null)
            {
                SalonAutomobila s = db.SalonAutomobilas.Find(sid);
                List<SalonAutomobila> salon = new List<SalonAutomobila>();
                salon.Add(s);
                ViewBag.SalonAutomobilaId = new SelectList(salon, "Id", "Naziv");
            }
            else
            {
                ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv");
            }

            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
;
            return View(ugovor);
        }

        // POST: Ugovors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SalonAutomobilaId,ProizvodjacId")] Ugovor ugovor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ugovor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProizvodjacId = new SelectList(db.Proizvodjacs, "Id", "Naziv", ugovor.ProizvodjacId);
            ViewBag.SalonAutomobilaId = new SelectList(db.SalonAutomobilas, "Id", "Naziv", ugovor.SalonAutomobilaId);
            return View(ugovor);
        }

        // GET: Ugovors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ugovor ugovor = db.Ugovors.Include(u => u.Proizvodjac).Include(u => u.SalonAutomobila).SingleOrDefault(x => x.Id == id);
            if (ugovor == null)
            {
                return HttpNotFound();
            }
            return View(ugovor);
        }

        // POST: Ugovors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ugovor ugovor = db.Ugovors.Find(id);
            db.Ugovors.Remove(ugovor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
