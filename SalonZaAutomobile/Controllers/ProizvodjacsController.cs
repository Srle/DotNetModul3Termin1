﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalonZaAutomobile.Models;

namespace SalonZaAutomobile.Controllers
{
    public class ProizvodjacsController : Controller
    {
        private SalonAutomobilaContext db = new SalonAutomobilaContext();

        // GET: Proizvodjacs
        public ActionResult Index()
        {
            return View(db.Proizvodjacs.ToList());
        }

        // GET: Proizvodjacs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvodjac proizvodjac = db.Proizvodjacs.Find(id);
            if (proizvodjac == null)
            {
                return HttpNotFound();
            }
            return View(proizvodjac);
        }

        // GET: Proizvodjacs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Proizvodjacs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv,Drzava,Grad")] Proizvodjac proizvodjac)
        {
            if (ModelState.IsValid)
            {
                db.Proizvodjacs.Add(proizvodjac);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proizvodjac);
        }

        // GET: Proizvodjacs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvodjac proizvodjac = db.Proizvodjacs.Find(id);
            if (proizvodjac == null)
            {
                return HttpNotFound();
            }
            return View(proizvodjac);
        }

        // POST: Proizvodjacs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv,Drzava,Grad")] Proizvodjac proizvodjac)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proizvodjac).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proizvodjac);
        }

        // GET: Proizvodjacs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proizvodjac proizvodjac = db.Proizvodjacs.Find(id);
            if (proizvodjac == null)
            {
                return HttpNotFound();
            }
            return View(proizvodjac);
        }

        // POST: Proizvodjacs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proizvodjac proizvodjac = db.Proizvodjacs.Find(id);
            db.Proizvodjacs.Remove(proizvodjac);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
